/*  ------------
    JQUERY POPUP
    ------------
*/ 
console.log("template_turbo_sodium");
(function($) {
  $.popup = function(){
    this.version = '0.5.0a';
    this.options = {};
    this.$popup = "";
    this.max_width=0;
  };
  $.popup.prototype.methods=function(method,parameter){
     switch(method){
       default:
       break;
     }
  };
  $.popup.prototype.init_max_width=function(){
    if(this.options.max_width>0){
      this.max_width= this.options.max_width; 
      this.$popup.find(".popup-content").css('max-width',this.max_width);
    }else{
      var elements=this.$popup.find("*");
      var max_width=0;
      elements.each(function(i,e){
        if($(e).attr("width")){
          if(max_width<parseInt($(e).attr("width")))
            max_width=parseInt($(e).attr("width"));
        }
      });
      if(max_width==0){
        max_width=800;
      }
      this.max_width=max_width;
      this.$popup.find(".popup-content").css('max-width',this.max_width);
    }
  };
  $.popup.prototype.init= function(options) {
     this.options=options;
     this.iframe=this.$el.parents(this.options.wrapper).find(this.options.content).find("iframe");
     this.iframe.attr("data-src",this.iframe.attr("src"));
     this.iframe.attr("src","");
     this.init_container();
     
  };
  $.popup.prototype.hide=function(){
        this.$popup.addClass("hide-content");
        var elem = this.$popup;
        var iframe = this.iframe;
        setTimeout(function(){ 
            elem.removeClass("hide-content");
            elem.removeClass("show-content");
            iframe.attr("src-back");
            iframe.attr("src","");
            $('body').css('overflow', 'auto');
        }, 200);
    }
  $.popup.prototype.show=function(){
     this.iframe.attr("src",this.iframe.attr("data-src"));
     this.$popup.addClass("show-content");
     $('body').css('overflow', 'hidden');
  }
  $.popup.prototype.toggle=function(){
     if(this.$popup.hasClass("show-content")){
        this.hide();       
     }else{
       this.show();
     }
  }
  $.popup.prototype.init_container= function() { 
     var temp_popup=this.$el.parents(this.options.wrapper).find(this.options.content);
     temp_popup.wrap("<div class='popup'></div>");
     temp_popup.wrap("<div class='popup-content'></div>");
     temp_popup.wrap("<div class='popup-content-wrapper'></div>");
     this.$popup=temp_popup.parents(".popup");
     this.$popup.find(".popup-content").append("<div class='popup-close'></div>");
     this.hide();
     this.init_events();
     this.init_max_width();
  };
  
  $.popup.prototype.init_events= function() {
     var self=this;
     self.$el.click(function(e){
       e.preventDefault();
       self.show();
       return false;
     });
    self.$popup.find(".popup-close").click(function(e){
       e.preventDefault();
       self.hide();
       return false;
     });
    self.$popup.click(function(e){
       e.preventDefault();
       self.hide();
       return false;
     });    
    self.$popup.find(".popup-content").click(function(e){
      e.stopPropagation();
    });
  };
  

    $.fn.popup = function(options,parameters,event) {
        // Set the default options
        var defaults = { max_width:0};
      
        if(typeof options !== 'string'){
          options = $.extend(defaults, options);
        }
        
        return this.each(function() {
            var popup;
          
            if(typeof options === 'string'){
                $(this).data("popup").methods(options,parameters);
            }else{
                popup = new $.popup();
                popup.$el=$(this);
                popup.init(options);
                $(this).data("popup",popup);
                
              
            }
            
        });
    };
})(jQuery);
var popup = $(".social-link-show-popup").popup({"content":".people-desc","wrapper":".people","max_width": 900});
var $iframes = $( "iframe" );

$iframes.each(function () {
  $( this ).data( "ratio", this.height / this.width )
    // Remove the hardcoded width &#x26; height attributes
});

/*  -------------------
    END OF POPUP IFRAME
    -------------------
*/

//Auto resize IFRAME
$( window ).resize( function () {
  $iframes.each( function() {
    // Get the parent container&#x27;s width
    var width = $( this ).parent().width();
    $( this ).width( width )
      .height( width * $( this ).data( "ratio" ) );
  });
// Resize to fix all iframes on page load.
}).resize();


var wow="";
$(document).ready(function () {
    if($("section.video-popup-module").length>0)
        $("div.mask").popup({"content":".video-popup","wrapper":".video-popup-module","max_width":850});
    //Fixed Menu
    fixedMenu();
    //Mobile Menu
    mobileMenuInit();
    
    //Scroll down in header
    headerScrollDown();
    //Scroll up in footer
    footerScrollUp();
    //WOW animation init
    wowInit();

    moveSection($('.hero-banner-form'), $('.form-placeholder'), $('.form-container'));
    moveSection($('.hero-banner-form'), $('.form-placeholder'), $('.features-container'));
    
    //ONE, TWO, THREE COLUMN HeroSlider
    smallSliderInit();

    //Homepage HeroSlider
    homeSliderInit();

    //Homepage testimonials
    testimonialsSliderInit();


    //Homepage Services
    var brake_array = {};
    brake_array["1200"] = 4;
    brake_array["992"] = 3;
    brake_array["768"] = 2;
    brake_array["469"] = 1;
    rowModuleBreaker(".homepage .services", ".box-services", brake_array, "first", "last");

    //Homepage Features
    var brake_array = {};
    brake_array["768"] = 2;
    brake_array["469"] = 1;
    rowModuleBreaker(".homepage .features", ".feature", brake_array, "first", "last");

    //Skills animation
    if ($(".loading-skills-wrapper").length > 0) {
        skillsInit();
    }
    questionAccordion();
    hideScrollTopOnSmallScreen();
    //test
    initBrandsSlider();

    // Execute for own scripts
    // Change / to # in a href links
    
    // Click for a in submenu
    if($('.second-menu').length > 0) {
            // console.log("Additional menu.");
            $('.additional-second-menu li a').click(function(e) {
            e.preventDefault();
            $('.slicknav_btn').click();
            goToSection($(this));
        });
        $('.second-menu ul li a').click(function(e) {
            e.preventDefault();
            goToSection($(this));
        });
        selectMenuItemByScroll();    
    }
    
});

$(window).load(function() {
    $("header.header .slicknav_nav > li:last-child a").removeClass('cta_button cta-primary');
});

// Modify script
// For submenu
function selectMenuItemByScroll() {
    var data = checkPositionSecFromTop();
    var window_top = 0;
    var window_width = $( window ).width();
    var active_key = '';
    var window_width = $( window ).width();
    $(window).on('resize', function(){
        data = checkPositionSecFromTop();
    });
    $(window).scroll(function(){
        if(window_width <= 971) {
            window_top = $(window).scrollTop() + 70;
        }
        else {
            window_top = $(window).scrollTop() + 123;
        }
        var last_key = '';
        for (var key in data) {
            if(data[key] - window_top <= 0) {
                last_key = key;
            }
        }
        if(last_key !== '') {
            if(last_key !== active_key) {
                active_key = last_key;
                $('.second-menu ul li a').each(function(index, el) {
                    $(this).removeClass('active');
                });
                $('.second-menu ul li a[href$="' + active_key + '"]').addClass('active');
            }
        }
        else {
            if(active_key !== '') {
                active_key = '';
                $('.second-menu ul li a').each(function(index, el) {
                    $(this).removeClass('active');
                });
            }
            
        }
    });
}
function checkPositionSecFromTop() {
    var data = {};
    $('.sec-scroll').each(function(index, el) {
        var name = $(this).attr('id');
        var position = $(this).offset().top;
        data[name] = position;
    });
    return data;
}
function changeLinkToCorrect() {
    if($('.second-menu ul li a').length) {
        $('.second-menu ul li a').each(function(){
            var temp = $(this).attr('href').replace("/", "#");
            $(this).attr('href', temp);
        });
    }
    if($('.additional-second-menu li a').length) {
        $('.additional-second-menu li a').each(function(){
            var temp = $(this).attr('href').replace("/", "#");
            $(this).attr('href', temp);
        });
    }
}
function goToSection(elem) {
    var nameOfSection = elem.attr('href').replace('#', '');
    var window_width = $( window ).width();
    if(window_width <= 971) {
        window_top = 70;
    }
    else {
        window_top = 123;
    }
    $("html, body").animate({
        scrollTop: $('#' + nameOfSection).offset().top - window_top
    }); 
}



// OLD Style from default template
$(window).load(function(){
   headerMenuFlyout();
   //$('img[src$=".svg"].svg-icon, .svg-icon img[src$=".svg"]').svgConvert();
});

function moveSection(elementToMove, firstPlaceholder, secondPalceholder) {
    $(window).resize(function() {
        if (window.innerWidth < 801 && firstPlaceholder.find(elementToMove).length == 1) {
            secondPalceholder.prepend(elementToMove);
            elementToMove.css('margin-bottom','20px');
        } else if (window.innerWidth >= 801 && secondPalceholder.find(elementToMove).length == 1) {
            firstPlaceholder.prepend(elementToMove);
            elementToMove.css('margin-bottom','0px');
        }
    });
}
    
function hideScrollTopOnSmallScreen() {
    $(window).resize(function () {
        if (window.innerHeight + 220 >= $("body").height()) {
            $(".footer .scroll-top").hide();
        } else {
            $(".footer .scroll-top").show();
        }
    });
    $(window).resize();
}
function wowInit() {
    
    wow = new WOW({
        mobile: false
    });
    wow.init();
}
function questionAccordion() {
    if ($("html.hs-inline-edit").length == 0) {
        $("section.question").each(function (i, e) {
            $(e).find(".panel-group").append($(e).find(".faq-box"));
            $(e).find(".hs_cos_wrapper_type_widget_container,.hs_cos_wrapper_type_custom_widget").remove();
        });
    }
    $("section.question .panel-group").attr("id", "accordion");
    $("section.question .panel-group .panel").each(function (i, e) {
        var href = $(e).find("h4.panel-title > a").attr("href");
        $(e).find("h4.panel-title > a").attr("href", href + (i + 1));

        var id = $(e).find(".panel-collapse").attr("id");
        $(e).find(".panel-collapse").attr("id", id + (i + 1));
    });
}
function headerScrollDown() {
    $(".mouse_scroll").click(function () {
        $("html, body").animate({
            scrollTop: $("#main_section").position().top - 70
        }, 1000);
        return false;
    });
}
function footerScrollUp() {
    $(".scroll-top").click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 500);
        return false;
    });
}
function testimonialsSliderInit() {
    $('.slick-testimonial > div > span').slick({
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 8000,
        speed: 1000,
        responsive: [{
            breakpoint: 767,
            settings: {
                dots: true,
                arrows: false
            }
        }]
    });
}
function headerMenuFlyout(){
    if($("header.header .hs-menu-flow-horizontal > ul").length>0){

    $("header.header .hs-menu-flow-horizontal > ul > li > ul > li > ul").css("top",0);
    $("header.header .hs-menu-flow-horizontal > ul > li > ul").css("top",70);

    $.each($("header.header .hs-menu-flow-horizontal > ul > li > ul > li > ul"),function(i,e){
        $(e).attr("data-parent-offset",-$(e).offset().top+70+parseInt($(window).scrollTop()));        
    });
        $.each($("header.header .hs-menu-flow-horizontal > ul li.hs-menu-depth-2"),function(i,e){
            if($(e).find(">ul").length>0){
                $(this).find(">ul").css("left",$(e).parents("ul:first").width());
            }
        });     
        $("header.header .hs-menu-flow-horizontal > ul > li").addClass("right"); 

    $("header.header .hs-menu-flow-horizontal > ul > li > ul > li > ul").css("top","");
    $("header.header .hs-menu-flow-horizontal > ul > li > ul").css("top","");
    var tooBig=false;
    $("header.header .hs-menu-flow-horizontal > ul > li > ul > li").mousemove(function(){       
       if(tooBig){           
            if(!$(this).hasClass("hover")){
                $("header.header .hs-menu-flow-horizontal > ul > li > ul > li").removeClass("hover");
                $("header.header .hs-menu-flow-horizontal > ul > li > ul > li > ul").css("top","");
            }
            $(this).find("ul").css("top",$(this).find("ul").data("parent-offset"));      
            $(this).addClass("hover");
            }else{
            $(this).find("ul").css("top",0);                      
            $(this).removeClass("hover");
}
    });
    
    $(window).resize(function(){
    $("header.header .hs-menu-flow-horizontal > ul > li > ul > li > ul").css("top",0);
    $("header.header .hs-menu-flow-horizontal > ul > li > ul").css("top",70);       
    $("header.header .hs-menu-flow-horizontal > ul > li > ul > li > ul").css("height","auto");
    tooBig=false;
    $.each($("header.header .hs-menu-flow-horizontal > ul > li > ul > li > ul"),function(i,e){
        if($(e).offset().top+$(e).height()>=(parseInt($(window).scrollTop())+parseInt(window.innerHeight))){
            tooBig=true;
        }
    });
    $("header.header .hs-menu-flow-horizontal > ul > li > ul > li > ul").css("top","");
    $("header.header .hs-menu-flow-horizontal > ul > li > ul").css("top","");
    $.each($("header.header .hs-menu-flow-horizontal > ul > li > ul > li"),function(i,e){
            $($(e).find("ul")).each(function(j,list){
                if(($(e).offset().left+$(e).width()+$(list).width())>window.innerWidth-25){
                    $(list).css("left",(-$(list).width()));                
                    $(e).addClass("left-arrow").removeClass("right-arrow");
                }else{
                    $(list).css("left",$(e).parents("ul:first").width());                                
                    $(e).addClass("right-arrow").removeClass("left-arrow");
                }
                if(tooBig){

                    if($(list).parents("ul:first").height()>$(list).height())
                        $(list).css("height",$(list).parents("ul:first").height()+2);
                }else{
                    $(list).css("height","auto");                    
                    $(list).css("opacity","");
                }
            });
            
      });
    });

$(window).resize();

}
}


/*function headerMenuFlyout(){
    if($("header.header .hs-menu-flow-horizontal > ul").length>0){
        $.each($("header.header .hs-menu-flow-horizontal > ul li.hs-menu-depth-2"),function(i,e){
            if($(e).find(">ul").length>0){
                $(this).find(">ul").css("left",$(this).width());
            }
        });     
        $("header.header .hs-menu-flow-horizontal > ul > li").addClass("right");
        $("header.header .hs-menu-flow-horizontal > ul li.hs-menu-depth-1 > ul, header.header .hs-menu-flow-horizontal > ul li.hs-menu-depth-2 > ul").each(function(){
            $(this).find(">li:last").after("<li class='scroll-bottom'></li>");
            $(this).find(">li:first").before("<li class='scroll-top'></li>");            
        });
        var hover=false;
        $("header.header .hs-menu-flow-horizontal > ul > li").mouseenter(function(){
            $("header.header .hs-menu-flow-horizontal > ul > li").find("ul").removeClass("scroll");
            $("header.header .hs-menu-flow-horizontal > ul > li").find("ul").height("auto");
            
                $(this).find("> ul").each(function(i,e){
                    $(e).find("li:nth(1)").css("margin-top",0);
                    $(this).attr("data-original-height",$(this).height());
                    if(($(this).offset().top+$(this).height())>window.innerHeight+$(window).scrollTop()){
                        $(this).addClass("scroll");
                        var diffrence=$(this).offset().top-$(window).scrollTop()+$(this).height()-window.innerHeight;
                        $(this).height($(this).height()-diffrence);
                     }           
                });
                $(this).find("> ul > li > ul").each(function(i,e){
                    $(e).find("li:nth(1)").css("margin-top",0);
                    $(this).attr("data-original-height",$(this).height());
                    if(($(this).offset().top+$(this).height())>window.innerHeight+$(window).scrollTop()){
                        $(this).addClass("scroll");
                        var diffrence=$(this).offset().top-$(window).scrollTop()+$(this).height()-window.innerHeight;
                        $(this).height($(this).height()-diffrence);
                     }           
                });
            var interval;
            $(".scroll > .scroll-bottom").mouseenter(function(){
                clearInterval(interval);
                var parent= $(this).parent();
                var scroll=$(this).parent().find('>li:nth(1)');
                var topS = parseInt(scroll.css("margin-top"));
                interval=setInterval(function(){
                    if((-1*topS)+parent.height()<parent.data("original-height")){
                        topS=topS-1;
                        scroll.css("margin-top",topS);
                    }
                },10);
            });
            $(".scroll > .scroll-bottom").mouseout(function(){
               clearInterval(interval);
            });
            $(".scroll > .scroll-top").mouseenter(function(){
                clearInterval(interval);
                var scroll=$(this).parent().find('>li:nth(1)');
                var topS = parseInt(scroll.css("margin-top"));
                interval=setInterval(function(){
                    if(topS<0){
                        topS=topS+1;
                        scroll.css("margin-top",topS);
                    }
                },10);
            });
            $(".scroll > .scroll-bottom").mouseout(function(){
               clearInterval(interval);
            });
        });
    $(window).resize(function(){
        

     $.each($("header.header .hs-menu-flow-horizontal > ul > li > ul > li"),function(i,e){
            $($(e).find("ul")).each(function(j,list){
                if(($(e).offset().left+$(e).width()+$(list).width())>window.innerWidth-25){
                    $(list).css("left",(-$(list).width()));                
                    $(e).addClass("left-arrow").removeClass("right-arrow");
                }else{
                    $(list).css("left",$(e).width());                                
                    $(e).addClass("right-arrow").removeClass("left-arrow");
                }
            });
            
      });
    });

$(window).resize();
}
}
*/
function mobileMenuInit() {
    if($("header.header .hs-menu-flow-horizontal > ul").length>0){
        $("header.header .hs-menu-flow-horizontal > ul").slicknav({
            prependTo: "header.header",
            label: "",
            allowParentLinks: true,
            init:function(){
                //$("header.header .slicknav_menu > ul").append("<li></li>");
                //$("header.header .slicknav_menu > ul > li:last").append($("header.header .search").clone(true));
                
                // Click for a in submenu
                if($('.second-menu').length > 0) {
                    // console.log("Additional menu.");
                    // Dodanie do menu mobilnego przycisku z menu desktopowego
                    $("header.header .slicknav_menu > .slicknav_nav:last").append('<li class="hs-menu-item hs-menu-depth-1"></li>');
                    $("header.header .slicknav_menu > .slicknav_nav li:last-child").append($('.header-container-wrapper .widget-type-global_group .container .widget-type-cta').clone(true).removeClass('span2'));
                    
                    var menuTitle = $('.second-menu').data('menu-title').trim().toLowerCase();
                    // console.log(menuTitle);
                    $('header.header .slicknav_menu > .slicknav_nav a').each(function(){
                        if($(this).text().trim().toLowerCase() == menuTitle) {
                            $(this).attr('role', 'menuitem').addClass('slicknav_item slicknav_row').css('outline', 'none').attr('aria-haspopup', 'true').append('<span class="slicknav_arrow">►</span>');
                            $(this).parent().addClass('hs-item-has-children slicknav_collapsed slicknav_parent').append($('.second-menu ul').clone(true).addClass('hs-menu-children-wrapper slicknav_hidden additional-second-menu').css('display', 'none').attr('role', 'menu'));
                        }
                    });
                    changeLinkToCorrect();
                }
            }
        });
    }
    $(".slicknav_btn").click(function (i) {
        if ($("header.header.fixed-small").length > 0) {
            $("header.header").removeClass("fixed-small");
        } else {
            $("header.header").addClass("fixed-small");
        }
    });
    
}
function homeSliderInit() {
    $(".main .homeslider > span").slick({
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: "linear",
        responsive: [{
            breakpoint: 767,
            settings: {
                dots: true,
                arrows: false
            }
        }]
    });
}

function smallSliderInit() {

    $(".sm-slider-main .slide-content > span").slick({
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: "linear",
        responsive: [{
            breakpoint: 767,
            settings: {
                dots: true,
                arrows: false
            }
        }]
    });
}
function initBrandsSlider(){
        if($(".brands-slider").length>0){
            $('.brands-slider').cutWrappers('.brands-slider__slide');
        $('.brands-slider').slick({
            autoplay: true,
            autoplaySpeed: 2000,
            slidesToShow:5,
            slidesToScroll: 5,
            arrows: true,
            dots: true,
              responsive: [
                {
                  breakpoint: 1150,
                  settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                  }
                }
,
                {
                  breakpoint: 900,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                  }
                }
,
                {
                  breakpoint: 760,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                  }
                }
            ]
        });
}
}
function fixedMenu() {
    if ($(".header--static").length < 1 && $(".contact-template .header").length == 0) {
        $(window).scroll(function () {
            var i = $(window).scrollTop();
            if (i > 0) {
                $("header.header").addClass("fixed");
                if ($(".menu-black").length > 0) {
                    $(".menu-black .header .black").css("display", "none");
                    $(".menu-black .header .white").css("display", "block");
                }
            } else {
                if (i == 0) {
                    $("header.header").removeClass("fixed");
                    if ($(".menu-black").length > 0) {
                        $(".menu-black .header .black").css("display", "block");
                        $(".menu-black .header .white").css("display", "none");
                    }
                }
            }
        });
    }
    if ($(".contact-template .header").length > 0) {
        $(".contact-template .header").addClass("fixed");
    }
}
function rowModuleBreaker(parent, css_selector, break_array, class_first_row, class_last_row) {
    var oldCols = 0;
    $(window).resize(function () {
        var cols = 0;
        var lastBreak = 0;
        for (var windowBreak in break_array) {
            if (window.innerWidth <= parseInt(windowBreak)) {
                cols = break_array[windowBreak];
                break;
            }
            lastBreak = windowBreak;
        }

        if (cols == 0) {
            cols = break_array[lastBreak];
        }
        if (oldCols == 0 || oldCols != cols) {
            $(parent).each(function (j, selector) {

                var old = oldCols;
                oldCols = cols;
                var inc = -1;
                var row = [];
                var length = $(selector).find(css_selector).length;
                $(selector).find(css_selector).each(function (i, e) {
                    if (i % cols == 0) {
                        inc++;
                        row = [];
                    }
                    row.push($(e).parent().clone(true));
                    $(e).parent().parent().addClass("old_remove");
                    if (i % cols == cols - 1) {
                        var rowContainer = $("<div class='row'></div>");
                        if (inc == 0) {
                            rowContainer.addClass(class_first_row);
                        }
                        if (i == length - 1) {
                            rowContainer.addClass(class_last_row);
                        }
                        rowContainer.append(row);
                        $(e).parent().parent().before(rowContainer);
                    }

                    if (i % cols < cols - 1 && i == length - 1) {
                        var rowContainer = $("<div class='row'></div>");
                        if (inc == 0) {
                            rowContainer.addClass(class_first_row);
                        }
                        rowContainer.addClass(class_last_row);
                        rowContainer.append(row);
                        $(e).parent().parent().before(rowContainer);
                    }
                    if ($(e).parent().parent().hasClass("hs_cos_wrapper_type_custom_widget")) {
                        $(e).parent().parent().remove();
                    }
                });
                $(selector).find(css_selector).parent().attr("class", "span" + 12 / cols);
                $(".old_remove").remove();
            });
        }
    });
    $(window).resize();
}

function skillsInit() {
    var checkBars = 0;
    $(window).scroll(function() {
        var scrollTop = $(window).scrollTop();
        if ($(".loading-skills-wrapper").length > 0) {
            loadingBars();
        }
    });
    $(window).scroll();

    function loadingBars() {
        var scrollTop = $(window).scrollTop();
        var positionBars = $(".loading-circle").offset().top;
        var windowHeight = $(window).height()
        var scrollBottom = scrollTop + windowHeight;
        if (scrollBottom - 150 > positionBars && checkBars == 0) {
            checkBars = 1;

            $("section.loading-circle .circle").circliful({
                animation: 1,
                animationStep: 5,
                foregroundBorderWidth: 3,
                backgroundBorderWidth: 3,
                foregroundColor: '#404040',
                backgroundColor: '#fff',
                fontColor: '#404040',
                percentageTextSize: 30,
                multiPercentage: 1
            });
            $("section.loading-circle .circle .timer").attr('y', '110');
        }
    };
}




(function ($) {
    $.fn.cutWrappers = function (targetSelector) {
        $(this).find(targetSelector).appendTo($(this));
        $(this).find('.hs_cos_wrapper_type_custom_widget').remove();
        $(this).find('.hs_cos_wrapper_type_widget_container').remove();
        $(this).find('.widget-span').remove();
    };
})(jQuery);
//$(".pricing-box__container").cutWrappers('.pricing-box');

    var keys=[];
function findString(text){
    return text.toLowerCase()==searched.toLowerCase();
}
var searched="";
$(".blog-listing>section").each(function(i,resource){
    searched=$(resource).data('type');
    if(searched!=""){
        if(keys.findIndex(findString)==-1){
            keys.push(searched);
        }
    }
});

$.each(keys,function(i,key){
    $("select[name='content_type']").append("<option>"+key+"</option>");
});
    $('.blog-listing').isotope({
  filter: function() {
    var content_type=$("select[name='content_type']").val();
    var all_topics=$("select[name='all_topics']").val();
    var requirements=2;
        if($(this).data("topic").search(all_topics)>-1){
            requirements--;
        }
        if($(this).data("type").search(content_type)>-1||$(this).data("type")!=""){
            requirements--;
        }
        if(requirements==0){
            return true;
        }else{
            return false;
        }
    },
      itemSelector: 'section',
      layoutMode: 'fitRows',
       masonry: {
          columnWidth: '50%'
        }
    });
$("select[name='content_type'],select[name='all_topics']").change(function(){
    var content_type=$("select[name='content_type']").val();
    var all_topics=$("select[name='all_topics']").val();

    $('.blog-listing').isotope({
  filter: function() {
    var requirements=2;
        if($(this).data("topic").search(all_topics)>-1){
            requirements--;
        }
        if($(this).data("type").search(content_type)>-1||$(this).data("type")!=""){
            requirements--;
        }
        if(requirements==0){
            return true;
        }else{
            return false;
        }
    },
      itemSelector: 'section',
      percentPosition:true,
      layoutMode: 'fitRows',
       masonry: {
          columnWidth: 50
        }
    });
});